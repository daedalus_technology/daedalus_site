var margin = {top: 10, left: 10, bottom: 10, right: 10}
  , width = parseInt(d3.select('#medmap').style('width'))
  , width = width - margin.left - margin.right
  , mapRatio = .5
  , height = width * mapRatio;

var projection = d3.geoMercator()
	.scale(width/2.75)
    // .scale(700)
    .center([10.5, 15]);
    // .translate([width / 2, height / 2])

var path = d3.geoPath().projection(projection);

var map_svg = d3.select('#medmap')
            .append('svg')
            .attr('width', width)
            .attr('height', height)
            .append('g')
            .attr('class', 'map');

d3.json('assets/geojson/medicine_countries.json', function(error, data) {
	if (error) throw error
	

	map_svg.append("g")
		.attr("class", "countries")
	    .selectAll("path")
	    .data(data.features)
	    .enter().append("path")
	        .attr("d", path)
	        .attr("class", "land")
      		.style('stroke', 'grey')
      		.style('stroke-width', 0.5)
      		.style("opacity",0.8)

});

d3.select(window).on('resize', resize);

function resize() {
    // adjust things when the window size changes
    width = parseInt(d3.select('#medmap').style('width'));
    width = width - margin.left - margin.right;
    height = width * mapRatio;

    // update projection
    projection
		.scale(width/2.75)
        // .translate([width / 2, height / 2])

    // resize the map container
    map_svg
        .style('width', width + 'px')
        .style('height', height + 'px');

    // resize the map
    map_svg.select('.land').attr('d', path);
    // map.selectAll('.state').attr('d', path);
}