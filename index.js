const express = require('express');
const app = express();
const nodemailer = require('nodemailer');
const router = express.Router()
const bodyParser = require('body-parser'); // Adding body parser for json parsing
const rssfeed = require('rss-to-json');

const listenPort = 80;

// Use forever library as Daemon process

// Create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'lewis@daedalustechnology.co.uk',
        pass: 'Phobos123'
    }
});

app.use(express.static(__dirname +'/daedalus_public'));

// Parses the text as URL encoded data and exposes the resulting object (containing the keys and values) on req.body
app.use(bodyParser.urlencoded({
    extended: true
}));

// Parses the text as JSON and exposes the resulting object on req.body.
app.use(bodyParser.json());


// Load latest Medium
 
rssfeed.load('https://medium.com/feed/@lewis_b', function(err, rss){
    // console.log(rss);
});


app.post('/contact_form/', function(request, response) {
	console.log('Posted to Contact')
	console.log(request.body)

	let mailOptions = {
		to: 'lewis@daedalustechnology.co.uk',
		from: request.body.email,
		subject: 'Contact Form Submission',
		text: request.body.message //, // plaintext body
	}

	console.log(mailOptions)

	// send mail with defined transport object
	transporter.sendMail(mailOptions, (error, info) => {
	    if (error) {
	        return console.log(error);
	    	response.send(error)
	    }
	    console.log('Message %s sent: %s', info.messageId, info.response);
	    response.send('Success')
	});

});

app.listen(listenPort, function(){
	console.log('Static hosting listening on port '+listenPort);
});